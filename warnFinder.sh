#!/bin/bash
#Какие ключенвые слова искать
ifToCheck=(WARN, ERROR)

function alert {
	USERID=(id_пользователя, id_пользователя)
	KEY="ключ:от_бота_телеграмм"

	for i in "${USERID[@]}"
	do
		URL="https://api.telegram.org/bot${KEY}/sendMessage"
		DATE="$(date "+%d.%m.%y %H:%M")"

		if [ -n "$SSH_CLIENT" ]; then
			TEXT="Найден <code>$*</code>
			🕐 <b>${DATE}</b>"

			curl -s -d "chat_id=$i&text=${TEXT}&disable_web_page_preview=true&parse_mode=HTML" $URL > /dev/null
		fi
	done
}

while true
do
	# цикл для всех значений массива
	for if in ${ifToCheck[*]}
	do
		# дернуть последние 30 строчек лога и найти ключевое слово без вывода в консоль
	    #if tail -n30 /home/server/logs/latest.log | grep $if >> /dev/null ; then
	    # с выводом строки где был найдено ключевое слово
	    if tail -n30 /home/server/logs/latest.log | grep $if ; then
			# оповещение в ТГ
            alert $if
            sleep 10s
    fi
	done
	sleep 1
done
