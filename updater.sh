#!/bin/bash
# Первый аргумент - версия ядра (например: /.updater.sh 1.15.2)
if [ -z $1 ] ; then
    echo "Укажите версию ядра"
    exit
fi
mc_version=$1

Reset='\033[0m'
Green='\033[0;32m'
Red='\033[0;31m'
Yellow='\033[0;33m'

# Получение текущей версии билда PaperSpigot'a
current_build=$(cat check.txt 2>/dev/null)
# Получение последней версии билда PaperSpigot'a
# Для работы нужно установить пакет 'jq' (apt-get install jq)
last_build="$(curl -s https://papermc.io/api/v1/paper/${mc_version}/latest | jq  -r '.build')"
# Вычисление разницы версий
let "missed_updates = last_build - current_build"

# last_build=$(curl -s "https://papermc.io/api/v1/paper/${mc_version}/latest" | awk -F'[:}]' '{print $(NF-1)}')

# Проверка наличия текстаинформации в файле 'check.txt'
if [ -f "check.txt" ]; then
    # Если текущая версия отличается от последней версии
	if [ $last_build != $current_build ]
	then
		echo -e "${Red}Найдено обновления ядра ${Reset}PaperSpigot ${mc_version}${Yellow}#${last_build} ${Red} (пропущено ${missed_updates} обновлений) ${Reset}"
		# Бекап текущего билда с указанием версии
		mv paper.jar paper_${current_build}.jar
		# Скачивание последнего билда PaperSpigot'a
		wget -q --show-progress --progress=bar -O paper.jar https://papermc.io/api/v1/paper/${mc_version}/latest/download
		echo -e "${Green}Ядро обновлено. ${Reset}"
		# Запись последней версии билда в файл
		echo $last_build > check.txt
		else
			echo -e "${Green}Обновлений ядра нет. ${Reset}"
	fi
else
	echo -e "${Red}Ядро не найдено. Скачиваю последний билд. ${Reset}PaperSpigot ${mc_version}${Yellow}#${last_build} ${Reset}" 
	wget -q --show-progress --progress=bar -O paper.jar https://papermc.io/api/v1/paper/${mc_version}/latest/download
	echo -e "${Green}Ядро обновлено. ${Reset}"
	echo $last_build > check.txt
fi